import java.io.*;
public class Matrix
{
   public static void main(String argv[]) throws IOException
   {
      int zahl, count, auswahl, zeile;
      String eingabe;
      
      count = 1;
      zeile = 1;
      
      BufferedReader input = new BufferedReader(new InputStreamReader (System.in));
      
      System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
      eingabe = input.readLine();
      auswahl = Integer.parseInt(eingabe);
      
      while(auswahl<2 || auswahl >9)
      {
         System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
         eingabe = input.readLine();
         auswahl = Integer.parseInt(eingabe);
      }
      
      for(zahl=0;zahl<=99;zahl++)
      {
         if(count<10)
         {
            if(zahl/10==auswahl || zahl%auswahl==0 || zahl%10==auswahl)
            {
               System.out.print("*  ");
            }
            else
            {
               System.out.print(zahl+" ");
               if(zahl<10)
               {
                  System.out.print(" ");
               }
            }
            count++;
         }
         else
         {
            if(zahl/10==auswahl || zahl%auswahl==0)
            {
               System.out.print("* ");
            }
            else
            {
               System.out.print(zahl+"");   
               System.out.print(" ");
            }
            System.out.println("");
            count=1;
            zeile++;
         }
      }
   }
}