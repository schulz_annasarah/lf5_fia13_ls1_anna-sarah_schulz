import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		// Gewicht abfrage
		System.out.println("Bitte geben sie ihr Gewicht in kg an:");
		double gewicht = tastatur.nextDouble();

		System.out.println("Bitte geben sie ihre K�rpergr��e in m an:");
		double groe�e = tastatur.nextDouble();

		System.out.printf("Sie haben einen BMI von:" + "%.1f\n", bmiBerechnen(gewicht, groe�e));

		System.out.println("Bitte geben sie ihr Geschlecht an mit w f�r weiblich und m f�r m�nnlich:");
		String geschlecht = tastatur.next();
		double bmi = bmiBerechnen(gewicht, groe�e);

		if (geschlecht.equals("m") && bmi < 20) {
			System.out.println("Untergewicht");

		} else if (geschlecht.equals("m") && bmi >= 20 && bmi <= 25) {
			System.out.println("Normalgewicht");

		} else if (geschlecht.equals("m") && bmi > 25) {
			System.out.println("�bergewicht");

		} else if (geschlecht.equals("w") && bmi < 19) {
			System.out.println("Untergewicht");

		} else if (geschlecht.equals("w") && bmi >= 19 && bmi <= 24) {
			System.out.println("Normalgewicht");

		} else
			System.out.println("�bergewicht");

	}

	public static double bmiBerechnen(double x, double y) {
		double bmi = x / (y * y);

		return bmi;

	}

}
