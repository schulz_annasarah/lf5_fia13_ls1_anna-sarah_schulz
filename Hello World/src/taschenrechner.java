import java.util.Scanner;

public class taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Willkommen bei Taschenrechner. Du kannst 2 Zahlen miteinander addieren, subtrahieren, multiplizieren oder dividieren");
		System.out.println("Bitte gebe die erste Zahl ein:");
		double zahl1= tastatur.nextDouble();
		System.out.println("Bitte gebe die zweite Zahl ein:");
		double zahl2 = tastatur.nextDouble();
		System.out.println("W�hle nun eine Rechenoperation und gib das jeweilige Symbol ein(+ , -, *,/):");
		String rechenOperation = tastatur.next();
		
		if (rechenOperation.equals("+")) {
			 double ergebnis = zahl1 + zahl2;
			 System.out.println(ergebnis);
			 
		}
		else if (rechenOperation.equals("-")) {
			double ergebnis = zahl1 - zahl2;
			 System.out.println(ergebnis);
		} else if (rechenOperation.equals("*")) {
			double ergebnis = zahl1 * zahl2;
			 System.out.println(ergebnis);
		}else if (rechenOperation.equals("/")) {
			double ergebnis = zahl1 / zahl2;
			 System.out.println(ergebnis);
		} else System.out.println("Das ist kein g�ltiges Rechenzeichen");

	}

}
