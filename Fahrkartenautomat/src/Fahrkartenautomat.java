﻿

import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		boolean neu = true;
		Scanner tastatur = new Scanner(System.in);
		do {

			double zuzahlenderBetrag = fahrkartenbestellungErfassen();

			double rückgabebetrag = fahrkartenBezahlen(zuzahlenderBetrag);

			fahrkartenAusgeben();

			rueckgeldAusgaben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
			neu = false;
			System.out.println("Um den Fahrkartenautomat erneut zu benutzen geben sie true ein");
			neu = tastatur.hasNext();
		} while (neu = true);

	}

	public static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);
		byte ticketanzahl;
		byte tarifEingabe;
		// String [] tarifwahl = new String[9];
		String[] tarifWahl = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "	Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		// man kannn den Array einfach erweitern um neue Tarife hinzuzufügen
		// jedem Tarif kann ein Ticketpreis zugeordent werden
		//double[] ticketpreis = new double[9];
		double[] ticketpreis = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		double tarifPreis = 0;

		double zuzahlenderBetrag;

		// Abfrage wie viele Tickets

		for (String i : tarifWahl) {
			System.out.println(i);

		}

		tarifEingabe = tastatur.nextByte();

		if (tarifEingabe == 0) {

			// tarifwahl[0] = "Einzelfahrschein Berlin AB [2,90 EUR] (0)";
			System.out.println("Ihre Wahl: " + tarifWahl[0] + "  "+ticketpreis[0]+"EUR");
			//tarifPreis = ticketpreis[0] = 2.90;
			tarifPreis = ticketpreis[0];

		} else if (tarifEingabe == 1) {
			// tarifWahl[0] = "Einzelfahrschein Berlin BC [3,30 EUR] (1)";
			System.out.println("Ihre Wahl: " + tarifWahl[1]+ "  "+ticketpreis[1]+"EUR");
			//tarifPreis = ticketpreis[1] = 3.30;
			tarifPreis = ticketpreis[1];
		} else if (tarifEingabe == 2) {
			// tarifwahl[0] = "Einzelfahrschein Berlin ABC [3,60 EUR] (2)";
			System.out.println("Ihre Wahl: " + tarifWahl[2]+ "  "+ticketpreis[2]+"EUR");
			//tarifPreis = ticketpreis[2] = 3.60;
			tarifPreis = ticketpreis[2];
		} else if (tarifEingabe == 3) {
			// tarifwahl[0] = "Kurzstrecke [1,90 EUR] (3)";
			System.out.println("Ihre Wahl: " + tarifWahl[3]+ "  "+ticketpreis[3]+"EUR");
			//tarifPreis = ticketpreis[3] = 1.90;
			tarifPreis = ticketpreis[3];
		} else if (tarifEingabe == 4) {
			// tarifwahl[0] = "Tageskarte Berlin AB [8,60 EUR] (4)";
			System.out.println("Ihre Wahl: " + tarifWahl[4]+ "  "+ticketpreis[4]+"EUR");
			//tarifPreis = ticketpreis[4] = 8.60;
			tarifPreis = ticketpreis[4];
		} else if (tarifEingabe == 5) {
			// tarifwahl[0] = "Tageskarte Berlin BC [9,00 EUR] (5)";
			System.out.println("Ihre Wahl: " + tarifWahl[5]+ "  "+ticketpreis[5]+"EUR");
			//tarifPreis = ticketpreis[5] = 9.00;
			tarifPreis = ticketpreis[5];
		} else if (tarifEingabe == 6) {
			// tarifwahl[0] = "Tageskarte Berlin ABC [9,60 EUR] (6)";
			System.out.println("Ihre Wahl: " + tarifWahl[6]+ "  "+ticketpreis[6]+"EUR");
			//tarifPreis = ticketpreis[6] = 9.60;
			tarifPreis = ticketpreis[6];
		} else if (tarifEingabe == 7) {
			// tarifwahl[0] = "Kleingruppen-Tageskarte Berlin AB [23,50 EUR] (7)";
			System.out.println("Ihre Wahl: " + tarifWahl[7]+ "  "+ticketpreis[7]+"EUR");
			//tarifPreis = ticketpreis[7] = 23.50;
			tarifPreis = ticketpreis[7];
		} else if (tarifEingabe == 8) {
			// tarifwahl[0] = "Einzelfahrschein Berlin BC [3,30 EUR] (1)";
			System.out.println("Ihre Wahl: " + tarifWahl[8]+ "  "+ticketpreis[8]+"EUR");
			//tarifPreis = ticketpreis[8] = 24.30;
			tarifPreis = ticketpreis[8];
		} else if (tarifEingabe == 9) {
			// tarifwahl[0] = "Einzelfahrschein Berlin BC [3,30 EUR] (1)";
			System.out.println("Ihre Wahl: " + tarifWahl[9]+ "  "+ticketpreis[9]+"EUR");
			//tarifPreis = ticketpreis[9] = 24.90;
			tarifPreis = ticketpreis[9];
		} else
			System.out.println(">>falsche Eingabe<<");

		System.out.println("Anzahl der Tickets: ");
		ticketanzahl = tastatur.nextByte();

		// Rechnung Preis gesamt

		return zuzahlenderBetrag = ticketanzahl * tarifPreis;

	}

	public static double fahrkartenBezahlen(double zuzahlenderBetrag) {

		double rückgabebetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		Scanner tastatur = new Scanner(System.in);

		System.out.printf("Zu zahlender Betrag (EURO) " + "%.2f\n", zuzahlenderBetrag);

		// Geldeinwurf

		eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuzahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f\n", (zuzahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.printf("Eingabe (mind. 5 Ct, hÃ¶chstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		return rückgabebetrag = eingezahlterGesamtbetrag - zuzahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {

		// Fahrscheinausgabe

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgaben(double rückgabebetrag) {

		// Rückgeldberechnung und -Ausgabe

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rueckgabebetrag in Höhe von %4.2f €%n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

	}
}
