import java.util.Arrays;

public class Aufgabe4 {

	public static void main(String[] args) {
		int[] lotto = { 3, 7, 12, 18, 27, 42 };
		int zahl1 = 12;
		int zahl2 = 13;
		boolean val = contains(lotto, zahl1);
		System.out.println("Lotto enth�lt " + zahl1 + "?\n" + val);

		boolean val2 = contains(lotto, zahl2);
		System.out.println("Lotto enth�lt " + zahl2 + "?\n" + val2);
		for (int i : lotto) {
			System.out.print(i + "  ");

		}
	}

	public static boolean contains(final int[] arr, final int key) {
		return Arrays.asList(arr).contains(key);
	}

}
